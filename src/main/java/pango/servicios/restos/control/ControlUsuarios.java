/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.UsuariosDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlUsuarios extends ActionSupport implements SessionAware{
    private UsuariosDTO usu, clonUsu;
    private List<UsuariosDTO> usuarios;
    private ServicioMaestro servicioMaestro;
    private String salida;
    private Long id;
    private String datos;
    
    private Map session;
    private UsuariosDTO usuario_sesion;

    public UsuariosDTO getUsu() {
        return usu;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    public void setUsu(UsuariosDTO usu) {
        this.usu = usu;
    }

    public UsuariosDTO getClonUsu() {
        return clonUsu;
    }

    public void setClonUsu(UsuariosDTO clonUsu) {
        this.clonUsu = clonUsu;
    }

    public List<UsuariosDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuariosDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

    
  

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    public String registrarUsuarios (){
        try {
            usu.setEstado(1);
            servicioMaestro.registrarObjeto(usu);
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        salida="El usuario "+usu.getNombre()+" ha sido registrado.";
        datos="usuarios";
        return SUCCESS;
    }
    
    public String consultaUsuarios (){
        try {
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            usuarios = (List<UsuariosDTO>)(List)servicioMaestro.consultarObjetos("usuarios");
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        return SUCCESS;
    }
    
    public String editarUsuario(){
        try {
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            datos="usuarios";
            clonUsu = (UsuariosDTO) servicioMaestro.buscarObjeto(id, "usuarios");
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    
    }
    
    public String guardarCambiosUsuario(){
        try {
            servicioMaestro.actualizarObjeto(usu);
            salida= "El usuario "+usu.getNombre()+" ha sido correctamente actualizado";
            datos="usuarios";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String eliminarUsuario (){
        try {
            servicioMaestro.eliminarObjeto(id, "usuarios");
            salida= "El usuario ha sido correctamente eliminado";
            datos="usuarios";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    
}
