/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pango.servicios.restos.modelo.dto.CategoriasDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlCategorias extends ActionSupport {
    private List<CategoriasDTO> listaCategorias;
    private ServicioMaestro servicioMaestro;
    private String salida, datos, tabla, nombre, select;
    private CategoriasDTO cate, cateClon;
    private Long id;

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CategoriasDTO getCate() {
        return cate;
    }

    public void setCate(CategoriasDTO cate) {
        this.cate = cate;
    }

    public CategoriasDTO getCateClon() {
        return cateClon;
    }

    public void setCateClon(CategoriasDTO cateClon) {
        this.cateClon = cateClon;
    }

    public List<CategoriasDTO> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriasDTO> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }
    
    public String agregarCat (){
        try {
            System.out.println("en evento crear categoria");
            cate = new CategoriasDTO (nombre);
            cate.setEstado(1);
            servicioMaestro.registrarObjeto(cate);
            listaCategorias = (List<CategoriasDTO>)(List)servicioMaestro.consultarObjetos("categorias");
            tabla = "<table class='tablaGrande invertirColor' ><thead><tr><th>NOMBRE</th><th>ACCIONES</th></tr></thead><tbody>";
            select = "<select name='prod.idCategoria.id' id='registrarProd_prod_idCategoria_id'>";
            
            for (int i = 0; i < listaCategorias.size(); i++) {
                tabla = tabla+"<tr><td>"+listaCategorias.get(i).getNombre()+"</td>"
                        +"<td><a class='botonBonito' onclick='editarCategoria("+listaCategorias.get(i).getId()+")' href='#'>Editar</a>"
                        +"<a class='botonBonito'  onclick='eliminarCategoria("+listaCategorias.get(i).getId()+")' href='#'>Eliminar</a></td></tr>";
                select= select +"<option value='"+listaCategorias.get(i).getId()+"'>"+listaCategorias.get(i).getNombre()+"</option>";
            }
            select = select+"</select>";
            tabla = tabla+"</tbody></table>";
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    } 
    
    
    public String eliminarCat (){
        try {
            System.out.println("en evento eliminar categoria");

            servicioMaestro.eliminarObjeto(id, "categorias");
            listaCategorias = (List<CategoriasDTO>)(List)servicioMaestro.consultarObjetos("categorias");
            tabla = "<table class='tablaGrande invertirColor' ><thead><tr><th>NOMBRE</th><th>ACCIONES</th></tr></thead><tbody>";
            select = "<select name='prod.idCategoria.id' id='registrarProd_prod_idCategoria_id'>";
            
            for (int i = 0; i < listaCategorias.size(); i++) {
                tabla = tabla+"<tr><td>"+listaCategorias.get(i).getNombre()+"</td>"
                        +"<td><a class='botonBonito' onclick='editarCategoria("+listaCategorias.get(i).getId()+")' href='#'>Editar</a>"
                        +"<a class='botonBonito'  onclick='eliminarCategoria("+listaCategorias.get(i).getId()+")' href='#'>Eliminar</a></td></tr>";
                select= select +"<option value='"+listaCategorias.get(i).getId()+"'>"+listaCategorias.get(i).getNombre()+"</option>";
            }
            select = select+"</select>";                
            tabla = tabla+"</tbody></table>";
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    } 
    
}
