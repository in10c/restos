/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.MesasDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlMesas extends ActionSupport implements SessionAware{
    private MesasDTO mesa;
    private MesasDTO clonMesa;
    private List<MesasDTO> mesas;
    private ServicioMaestro servicioMaestro;
    private String salida;
    private Long id;
    private String datos;
    private Map session;
    private UsuariosDTO usuario_sesion;

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

    public MesasDTO getClonMesa() {
        return clonMesa;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public void setClonMesa(MesasDTO clonMesa) {
        this.clonMesa = clonMesa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    public String getSalida() {
        return salida;
    }

    public List<MesasDTO> getMesas() {
        return mesas;
    }

    public void setMesas(List<MesasDTO> mesas) {
        this.mesas = mesas;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public MesasDTO getMesa() {
        return mesa;
    }

    public void setMesa(MesasDTO mesa) {
        this.mesa = mesa;
    }
    
    public String registrarMesa (){
        try {
            mesa.setEstado(1);
            servicioMaestro.registrarObjeto(mesa);
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        datos="mesas";
        salida="La mesa "+mesa.getNombre()+" ha sido registrada.";
        return SUCCESS;
    }
    
    public String consultaMesas (){
        try {
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            System.out.println("estamos en control");
            mesas = (List<MesasDTO>)(List)servicioMaestro.consultarObjetos("mesas");
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        return SUCCESS;
    }
    
    public String editarMesa(){
        try {
            datos="mesas";
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            clonMesa = (MesasDTO)servicioMaestro.buscarObjeto(id, "mesas");
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    
    }
    
    public String guardarCambiosMesa(){
        try {
            servicioMaestro.actualizarObjeto(mesa);
            salida= "La mesa "+mesa.getNombre()+" ha sido correctamente actualizada";
            datos="mesas";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String eliminarMesa (){
        try {
            servicioMaestro.eliminarObjeto(id, "mesas");
            salida= "La mesa ha sido correctamente eliminada";
            datos="mesas";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
}
