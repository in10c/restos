/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import com.opensymphony.xwork2.ActionSupport;
import static java.lang.Integer.parseInt;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.*;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlOrden extends ActionSupport implements SessionAware{
    private List<MesasDTO> listaMesas;
    private List<CategoriasDTO> listaCategorias;
    private List<AreasDTO> listaAreas;
    private List<ProductosDTO> listaProductos;
    private List<ClientesDTO> listaClientes;
    private List<OrdenesDTO> listaOrdenes;
    private List<DetallesOrdenesDTO> listaDetallesOrdenes;
    private ServicioMaestro servicioMaestro;
    private Integer  tipo, cantidad, idAux;
    private Float precUnit, precTotal;
    private Long idMesa, idCliente, idUsuario, idOrden, idProd;
    private MesasDTO mesa;
    private String salida;
    private ClientesDTO cliente;
    private UsuariosDTO usuario;
    private OrdenesDTO orden;
    private ProductosDTO prod;
    private DetallesOrdenesDTO detOrd;
    private Map session;
    private UsuariosDTO usuario_sesion;

    public List<OrdenesDTO> getListaOrdenes() {
        return listaOrdenes;
    }

    public Integer getIdAux() {
        return idAux;
    }

    public void setIdAux(Integer idAux) {
        this.idAux = idAux;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public void setListaOrdenes(List<OrdenesDTO> listaOrdenes) {
        this.listaOrdenes = listaOrdenes;
    }

    public List<DetallesOrdenesDTO> getListaDetallesOrdenes() {
        return listaDetallesOrdenes;
    }

    public void setListaDetallesOrdenes(List<DetallesOrdenesDTO> listaDetallesOrdenes) {
        this.listaDetallesOrdenes = listaDetallesOrdenes;
    }

    
    
    public Integer getCantidad() {
        return cantidad;
    }

    public DetallesOrdenesDTO getDetOrd() {
        return detOrd;
    }

    public void setDetOrd(DetallesOrdenesDTO detOrd) {
        this.detOrd = detOrd;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Float getPrecUnit() {
        return precUnit;
    }

    public void setPrecUnit(Float precUnit) {
        this.precUnit = precUnit;
    }

    public Float getPrecTotal() {
        return precTotal;
    }

    public void setPrecTotal(Float precTotal) {
        this.precTotal = precTotal;
    }

    public Long getIdProd() {
        return idProd;
    }

    public void setIdProd(Long idProd) {
        this.idProd = idProd;
    }

    public ProductosDTO getProd() {
        return prod;
    }

    public void setProd(ProductosDTO prod) {
        this.prod = prod;
    }

    public Long getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(Long idOrden) {
        this.idOrden = idOrden;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    
    
    public OrdenesDTO getOrden() {
        return orden;
    }

    public List<ClientesDTO> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<ClientesDTO> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public void setOrden(OrdenesDTO orden) {
        this.orden = orden;
    }

    public MesasDTO getMesa() {
        return mesa;
    }

    public void setMesa(MesasDTO mesa) {
        this.mesa = mesa;
    }

    public ClientesDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClientesDTO cliente) {
        this.cliente = cliente;
    }

    public UsuariosDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuariosDTO usuario) {
        this.usuario = usuario;
    }

    public Long getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Long idMesa) {
        this.idMesa = idMesa;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

    public List<MesasDTO> getListaMesas() {
        return listaMesas;
    }

    public void setListaMesas(List<MesasDTO> listaMesas) {
        this.listaMesas = listaMesas;
    }

    public List<CategoriasDTO> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriasDTO> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public List<AreasDTO> getListaAreas() {
        return listaAreas;
    }

    public void setListaAreas(List<AreasDTO> listaAreas) {
        this.listaAreas = listaAreas;
    }

    public List<ProductosDTO> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<ProductosDTO> listaProductos) {
        this.listaProductos = listaProductos;
    }

    
    
    public String pizarraPedidos(){
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        try {
            listaCategorias = (List<CategoriasDTO>)(List)servicioMaestro.consultarObjetos("categorias");
            listaAreas = (List<AreasDTO>)(List)servicioMaestro.consultarObjetos("areas");
            listaMesas = (List<MesasDTO>)(List)servicioMaestro.consultarObjetos("mesas");
            listaProductos = (List<ProductosDTO>)(List)servicioMaestro.consultarObjetos("productos");
            listaClientes = (List<ClientesDTO>)(List)servicioMaestro.consultarObjetos("clientes");
            return SUCCESS; 
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
         //consulto lista de categorias para llenar en combo
        
    }
    
    public String pizarraCocina(){
        return SUCCESS;        
    }
    
    public String entregarOrden () {
        try {
            servicioMaestro.eliminarObjeto(idOrden, "ordenes");
            listaOrdenes = (List<OrdenesDTO>)(List)servicioMaestro.consultarObjetos("ordenes");
            //listaDetallesOrdenes = (List<DetallesOrdenesDTO>)(List)servicioMaestro.consultarObjetos("detallesordenes");
            salida = "";
            for (int i = 0; i < listaOrdenes.size(); i++) {
                listaDetallesOrdenes = servicioMaestro.consulDetallesFiltrados(listaOrdenes.get(i).getId());
                salida = salida+"<div><h2>Orden para la mesa "+listaOrdenes.get(i).getIdMesa().getNombre()+"</h2>" +
                        "<ul>"; 
                for (int j = 0; j < listaDetallesOrdenes.size(); j++) {
                    salida= salida+"<li><span class='canti'>"+listaDetallesOrdenes.get(j).getCantidad()+" x </span>"+listaDetallesOrdenes.get(j).getIdProd().getNombre()+
"                    <span style='float:right; padding-right: 10px; font-size:1.3em;'>" +
"                        <a href='#' class='tachaLinea' style='color:#7E397A;'><i class='fa fa-check-circle'></i></a></span></li>"
                            +"<li class='tipoEnvio'><i class='fa fa-cutlery'></i> "+listaDetallesOrdenes.get(j).getIdProd().getIdArea().getNombre()+"</li>";
                }
                salida= salida+"</ul>"
                        +"<div class='contadorOrden'>" +
                            "<p class='contadores subtituloMini'>Activo por: " ;
                
                            Date da = new Date();
                            SimpleDateFormat formatM = new SimpleDateFormat("mm");
                            SimpleDateFormat formatS = new SimpleDateFormat("ss");

                            String minc = formatM.format(da);
                            String secc = formatS.format(da);
                            int mina = parseInt(minc);
                            int seca = parseInt(secc);
                            int sega = (mina*60)+seca;

                            
                                
                        String fecha = listaOrdenes.get(i).getTiempoRegistro().toString();    
                        String sub ="";
                        int pos = fecha.indexOf(' ')+4;
                        sub = fecha.substring(pos, fecha.length()-2);
                        String minp = sub.substring(0, sub.indexOf(':'));
                        String secp = sub.substring( sub.indexOf(':')+1);
                        int minob = parseInt(minp);
                        int secob = parseInt(secp);
                        
                        int segob = (minob*60)+secob;
                        int dif = sega-segob;
                        
                            salida = salida+"<span class='minu'>"+(dif/60)+"</span>:<span class='secu'>"+(dif%60)+"</span></p>" +
                        "</div>" +
                        "<div class='abajoOrden'>" +
                            "<a class='entregar botonBonito' onclick='entregar("+listaOrdenes.get(i).getId()+")' style='width:100px; text-align: center; color: #7E397A;' href='#'>ENTREGADO</a>" +
                        "</div>" +
                "</div>";
            }
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String guardarOrden(){
        try {
            System.out.println("entrando a registrar orden");
            mesa = (MesasDTO)servicioMaestro.buscarObjeto(idMesa, "mesas");
            usuario = (UsuariosDTO)servicioMaestro.buscarObjeto(idUsuario, "usuarios");
            if(idCliente==0){
                System.out.println("el cliente es id = 0");
                orden = new OrdenesDTO (mesa, null, usuario);
            } else {
                System.out.println("el cliente si tiene id");
                cliente = (ClientesDTO)servicioMaestro.buscarObjeto(idCliente, "clientes");
                orden = new OrdenesDTO (mesa, cliente, usuario);
            }
            System.out.println("enviamos a registrar la orden");
            servicioMaestro.registrarObjeto(orden);
            servicioMaestro=null;
            return SUCCESS; 
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
        
    }
    
    public String guardarDetalle(){
        try {
            System.out.println("entrando a registrar detalle orden");
            orden = (OrdenesDTO)servicioMaestro.buscarObjeto(idOrden, "ordenes");
            prod = (ProductosDTO)servicioMaestro.buscarObjeto(idProd, "productos");
            detOrd = new DetallesOrdenesDTO(orden, prod, cantidad, precUnit, precTotal);
            System.out.println("enviamos a registrar detalle de la orden");
            servicioMaestro.registrarObjeto(detOrd);
            servicioMaestro=null;
            return SUCCESS; 
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
        
    } 
    
   

    public String actualizaOrdenes (){
        try {
            System.out.println("buscando ordenes ");
            listaOrdenes = (List<OrdenesDTO>)(List)servicioMaestro.consultarObjetos("ordenes");
            //listaDetallesOrdenes = (List<DetallesOrdenesDTO>)(List)servicioMaestro.consultarObjetos("detallesordenes");
            salida = "";
            for (int i = 0; i < listaOrdenes.size(); i++) {
                listaDetallesOrdenes = servicioMaestro.consulDetallesFiltrados(listaOrdenes.get(i).getId());
                salida = salida+"<div><h2>Orden para la mesa "+listaOrdenes.get(i).getIdMesa().getNombre()+"</h2>" +
                        "<ul>"; 
                for (int j = 0; j < listaDetallesOrdenes.size(); j++) {
                    salida= salida+"<li><span class='canti'>"+listaDetallesOrdenes.get(j).getCantidad()+" x </span>"+listaDetallesOrdenes.get(j).getIdProd().getNombre()+
"                    <span style='float:right; padding-right: 10px; font-size:1.3em;'>" +
"                        <a href='#' class='tachaLinea' style='color:#7E397A;'><i class='fa fa-check-circle'></i></a></span></li>"
                            +"<li class='tipoEnvio'><i class='fa fa-cutlery'></i> "+listaDetallesOrdenes.get(j).getIdProd().getIdArea().getNombre()+"</li>";
                }
                salida= salida+"</ul>"
                        +"<div class='contadorOrden'>" +
                            "<p class='contadores subtituloMini'>Activo por: " ;
                
                            Date da = new Date();
                            SimpleDateFormat formatM = new SimpleDateFormat("mm");
                            SimpleDateFormat formatS = new SimpleDateFormat("ss");

                            String minc = formatM.format(da);
                            String secc = formatS.format(da);
                            int mina = parseInt(minc);
                            int seca = parseInt(secc);
                            int sega = (mina*60)+seca;

                            
                                
                        String fecha = listaOrdenes.get(i).getTiempoRegistro().toString();    
                        String sub ="";
                        int pos = fecha.indexOf(' ')+4;
                        sub = fecha.substring(pos, fecha.length()-2);
                        String minp = sub.substring(0, sub.indexOf(':'));
                        String secp = sub.substring( sub.indexOf(':')+1);
                        int minob = parseInt(minp);
                        int secob = parseInt(secp);
                        
                        int segob = (minob*60)+secob;
                        int dif = sega-segob;
                        
                            salida = salida+"<span class='minu'>"+(dif/60)+"</span>:<span class='secu'>"+(dif%60)+"</span></p>" +
                        "</div>" +
                        "<div class='abajoOrden'>" +
                            "<a class='entregar botonBonito' onclick='entregar("+listaOrdenes.get(i).getId()+")' style='width:100px; text-align: center; color: #7E397A;' href='#'>ENTREGADO</a>" +
                        "</div>" +
                "</div>";
            }
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    
}
