/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.ClientesDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlClientes extends ActionSupport implements SessionAware{
    private ClientesDTO clien, clonClien;
    private List<ClientesDTO> clientes;
    private ServicioMaestro servicioMaestro;
    private String salida;
    private Long id;
    private String datos;
    private Map session;
    private UsuariosDTO usuario_sesion;

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }



    public ClientesDTO getClien() {
        return clien;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public void setClien(ClientesDTO clien) {
        this.clien = clien;
    }

    public ClientesDTO getClonClien() {
        return clonClien;
    }

    public void setClonClien(ClientesDTO clonClien) {
        this.clonClien = clonClien;
    }

    public List<ClientesDTO> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClientesDTO> clientes) {
        this.clientes = clientes;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
     public String registrarCliente (){
        try {
            clien.setEstado(1);
            
            servicioMaestro.registrarObjeto(clien);
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        datos="clientes";
        salida="El cliente "+clien.getNombre()+" ha sido registrado.";
        return SUCCESS;
    }
    
    public String consultaClientes (){
        try {
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            clientes = (List<ClientesDTO>)(List)servicioMaestro.consultarObjetos("clientes");
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
        
    }
    
    public String editarCliente(){
        try {
            datos="clientes";
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            clonClien = (ClientesDTO) servicioMaestro.buscarObjeto(id, "clientes");
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    
    }
    
    public String guardarCambiosCliente(){
        try {
            servicioMaestro.actualizarObjeto(clien);
            salida= "El cliente "+clien.getNombre()+" ha sido correctamente actualizado";
            datos="clientes";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String eliminarCliente (){
        try {
            servicioMaestro.eliminarObjeto(id, "clientes");
            salida= "El cliente ha sido correctamente eliminado";
            datos="clientes";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
}
