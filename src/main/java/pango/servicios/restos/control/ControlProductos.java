/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.AreasDTO;
import pango.servicios.restos.modelo.dto.CategoriasDTO;
import pango.servicios.restos.modelo.dto.ProductosDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlProductos extends ActionSupport implements SessionAware{
    private ProductosDTO prod, clonProd;
    private List<ProductosDTO> productos;
    private String salida, datos;
    private ServicioMaestro servicioMaestro;
    private File imagen;
    private String imagenContentType;
    private String imagenFileName;
    private Integer tipoMenu;
    private Long idCategoria, idArea;
    private List<CategoriasDTO> listaCategorias;
    private List<AreasDTO> listaAreas;
    
    
    private Map session;
    private UsuariosDTO usuario_sesion;

    public Long getIdCategoria() {
        return idCategoria;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    
    
    public Integer getTipoMenu() {
        return tipoMenu;
    }

    public void setTipoMenu(Integer tipoMenu) {
        this.tipoMenu = tipoMenu;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Long getIdArea() {
        return idArea;
    }

    public void setIdArea(Long idArea) {
        this.idArea = idArea;
    }

    
    
    public File getImagen() {
        return imagen;
    }

    public List<CategoriasDTO> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriasDTO> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public List<AreasDTO> getListaAreas() {
        return listaAreas;
    }

    public void setListaAreas(List<AreasDTO> listaAreas) {
        this.listaAreas = listaAreas;
    }

    public void setImagen(File imagen) {
        this.imagen = imagen;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public String getImagenFileName() {
        return imagenFileName;
    }

    public void setImagenFileName(String imagenFileName) {
        this.imagenFileName = imagenFileName;
    }

    public ProductosDTO getProd() {
        return prod;
    }

    public void setProd(ProductosDTO prod) {
        this.prod = prod;
    }

    public ProductosDTO getClonProd() {
        return clonProd;
    }

    public void setClonProd(ProductosDTO clonProd) {
        this.clonProd = clonProd;
    }

    public List<ProductosDTO> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductosDTO> productos) {
        this.productos = productos;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

   

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    private Long id;
    
     public String formRegProd (){
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        try {
            listaCategorias = (List<CategoriasDTO>)(List)servicioMaestro.consultarObjetos("categorias");
            listaAreas = (List<AreasDTO>)(List)servicioMaestro.consultarObjetos("areas");
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        datos="prod";
        return SUCCESS; 
         //consulto lista de categorias para llenar en combo
        

    }
    
    
    public String registrarProd (){
        try {
            if (imagen != null) {
                System.out.println("Hay algo en la variable de imagen");
                Calendar date = Calendar.getInstance();
                String dTmp = Long.toString(date.getTimeInMillis());
                String destino = "/home/in10c/NetBeansProjects/RestOS/src/main/webapp/subidas/fotosProd/";
                System.out.println("dest : " + destino + dTmp + imagenFileName);

                File destFile = new File(destino, dTmp + imagenFileName);
                FileUtils.copyFile(imagen, destFile);
                System.out.println("Imagen subida con exito");
                prod.setImagen(dTmp+imagenFileName);
            }
            prod.setEstado(1);
            servicioMaestro.registrarObjeto(prod);
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        salida="El producto "+prod.getNombre()+" ha sido registrado.";
        
        datos="productos";
        return SUCCESS;
    }
    
    public String consultaProd (){
        try {
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            System.out.println("estamos en control.");
            productos = (List<ProductosDTO>)(List)servicioMaestro.consultarObjetos("productos");
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
        }
        return SUCCESS;
    }
    
    public String editarProd(){
        try {
            datos="prod";
            usuario_sesion = (UsuariosDTO) this.session.get("usuario");
            listaCategorias = (List<CategoriasDTO>)(List)servicioMaestro.consultarObjetos("categorias");
            listaAreas = (List<AreasDTO>)(List)servicioMaestro.consultarObjetos("areas");
            clonProd= (ProductosDTO) servicioMaestro.buscarObjeto(id, "productos");
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    
    }
    
    public String guardarCambiosProd(){
        try {
            if (imagen != null) {
                System.out.println("Hay algo en la variable de imagen");
                Calendar date = Calendar.getInstance();
                String dTmp = Long.toString(date.getTimeInMillis());
                String destino = "/home/in10c/NetBeansProjects/RestOS/src/main/webapp/subidas/fotosProd/";
                System.out.println("dest : " + destino + dTmp + imagenFileName);

                File destFile = new File(destino, dTmp + imagenFileName);
                FileUtils.copyFile(imagen, destFile);
                System.out.println("Imagen subida con exito");
                prod.setImagen(dTmp+imagenFileName);
            }
            servicioMaestro.actualizarObjeto(prod);
            salida= "El producto "+prod.getNombre()+" ha sido correctamente actualizado";
            datos="productos";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String eliminarProd (){
        try {
            servicioMaestro.eliminarObjeto(id, "productos");
            salida= "El producto ha sido correctamente eliminado";
            datos="productos";
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    public String buscarProductosFiltrados (){
        try {
            System.out.println("intentando buscar prod filtrados");
            productos = servicioMaestro.consultarProductosFiltrados(idCategoria, tipoMenu);
            salida="";
            for (int i = 0; i < productos.size(); i++) {
                salida = salida+" <div id='prod"+(i+1)+"' data-id='"+productos.get(i).getId()+"' data-imagen='"+productos.get(i).getImagen()+"' data-nombre='"+productos.get(i).getNombre()+"'" 
                +"data-contador='"+(i+1)+"'  data-precio='"+productos.get(i).getPrecio()+"'  class='producto' style='background: url(\"/RestOS/subidas/fotosProd/"+productos.get(i).getImagen()+"\"); background-size: 100% 100%;'>"                        
                    +"<div class='legenda'>"
                        +"<span class='tituloProd'>"+productos.get(i).getNombre()+"</span></div>"
                    +" <div id='wrap"+(i+1)+"' class='wrapBotones'>"
                        +"<a onclick='agregarAOrden("+(i+1)+")' href='#'><i class='fa fa-cutlery'></i></a>"
                        +"<a href='#'><i class='fa fa-info-circle'></i></a></div>  </div>";       
            }
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    }
    
    
}
