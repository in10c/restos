/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.control;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import pango.servicios.restos.modelo.dto.AreasDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ControlAreas extends ActionSupport {
    private List<AreasDTO> listaAreas;
    private ServicioMaestro servicioMaestro;
    private String  datos, tabla, nombre, select;
    private AreasDTO area, areaClon;
    private Long id;

    public List<AreasDTO> getListaAreas() {
        return listaAreas;
    }

    public void setListaAreas(List<AreasDTO> listaAreas) {
        this.listaAreas = listaAreas;
    }

    public ServicioMaestro getServicioMaestro() {
        return servicioMaestro;
    }

    public void setServicioMaestro(ServicioMaestro servicioMaestro) {
        this.servicioMaestro = servicioMaestro;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public AreasDTO getArea() {
        return area;
    }

    public void setArea(AreasDTO area) {
        this.area = area;
    }

    public AreasDTO getAreaClon() {
        return areaClon;
    }

    public void setAreaClon(AreasDTO areaClon) {
        this.areaClon = areaClon;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String agregarArea(){
        try {
            System.out.println("en evento crear area");
            area = new AreasDTO (nombre);
            area.setEstado(1);
            servicioMaestro.registrarObjeto(area);
            listaAreas = (List<AreasDTO>)(List)servicioMaestro.consultarObjetos("areas");
            tabla = "<table class='tablaGrande invertirColor' ><thead><tr><th>NOMBRE</th><th>ACCIONES</th></tr></thead><tbody>";
            select = "<select name='prod.idArea.id' id='registrarProd_prod_idArea_id'>";
            
            for (int i = 0; i < listaAreas.size(); i++) {
                tabla = tabla+"<tr><td>"+listaAreas.get(i).getNombre()+"</td>"
                        +"<td><a class='botonBonito' onclick='editarArea("+listaAreas.get(i).getId()+")' href='#'>Editar</a>"
                        +"<a class='botonBonito'  onclick='eliminarArea("+listaAreas.get(i).getId()+")' href='#'>Eliminar</a></td></tr>";
                select= select +"<option value='"+listaAreas.get(i).getId()+"'>"+listaAreas.get(i).getNombre()+"</option>";
            }
            select = select+"</select>";
            tabla = tabla+"</tbody></table>";
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    } 
    
    
    public String eliminarArea (){
        try {
            System.out.println("en evento eliminar area");

            servicioMaestro.eliminarObjeto(id, "areas");
            listaAreas = (List<AreasDTO>)(List)servicioMaestro.consultarObjetos("areas");
            tabla = "<table class='tablaGrande invertirColor' ><thead><tr><th>NOMBRE</th><th>ACCIONES</th></tr></thead><tbody>";
            select = "<select name='prod.idArea.id' id='registrarProd_prod_idArea_id'>";
            
            for (int i = 0; i < listaAreas.size(); i++) {
                tabla = tabla+"<tr><td>"+listaAreas.get(i).getNombre()+"</td>"
                        +"<td><a class='botonBonito' onclick='editarArea("+listaAreas.get(i).getId()+")' href='#'>Editar</a>"
                        +"<a class='botonBonito'  onclick='eliminarArea("+listaAreas.get(i).getId()+")' href='#'>Eliminar</a></td></tr>";
                select= select +"<option value='"+listaAreas.get(i).getId()+"'>"+listaAreas.get(i).getNombre()+"</option>";
            }
            select = select+"</select>";
            tabla = tabla+"</tbody></table>";
            servicioMaestro=null;
            return SUCCESS;
        } catch (Exception ex) {
            System.out.println("ERROR: "+ex.getMessage()); //para ver en la consola de jboss el error
            return ERROR;
        }
    } 
    
}
