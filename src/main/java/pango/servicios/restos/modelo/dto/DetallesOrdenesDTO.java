package pango.servicios.restos.modelo.dto;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author in10c
 */
public class DetallesOrdenesDTO {
    private Long id;
    private OrdenesDTO idOrden;
    private ProductosDTO idProd;
    private Integer cantidad;
    private Float precioUnitario, precioTotal;
    
    public DetallesOrdenesDTO(OrdenesDTO idOrden, ProductosDTO idProd, Integer cantidad, Float precioUnitario, Float precioTotal){
        this.idOrden = idOrden;
        this.idProd = idProd;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.precioTotal = precioTotal;
    }
    
    public DetallesOrdenesDTO(){
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrdenesDTO getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(OrdenesDTO idOrden) {
        this.idOrden = idOrden;
    }

    public ProductosDTO getIdProd() {
        return idProd;
    }

    public void setIdProd(ProductosDTO idProd) {
        this.idProd = idProd;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Float precioTotal) {
        this.precioTotal = precioTotal;
    }
    
    
    
}
