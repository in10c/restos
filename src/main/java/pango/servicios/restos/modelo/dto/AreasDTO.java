/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.modelo.dto;

/**
 *
 * @author in10c
 */
public class AreasDTO {
    private Long id;
    private String nombre;
    private Integer estado;
    
    public AreasDTO() {
        
    }
    
    public AreasDTO(Long id, String nombre){
        this.id = id;
        this.nombre = nombre;
        this.estado = 1;
    }
    
    public AreasDTO(String nombre){
        this.nombre = nombre;
        this.estado = 1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
}
