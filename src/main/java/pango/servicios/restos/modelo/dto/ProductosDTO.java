/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.modelo.dto;

/**
 *
 * @author in10c
 */
public class ProductosDTO {
    private Long id;
    private String nombre, descripcion, imagen;
    private Float precio;
    private Integer estado, tipoMenu;
    private CategoriasDTO idCategoria;
    private AreasDTO idArea;
    
    public ProductosDTO(){
        
    }

    public Integer getTipoMenu() {
        return tipoMenu;
    }

    public void setTipoMenu(Integer tipoMenu) {
        this.tipoMenu = tipoMenu;
    }

    public CategoriasDTO getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(CategoriasDTO idCategoria) {
        this.idCategoria = idCategoria;
    }

    public AreasDTO getIdArea() {
        return idArea;
    }

    public void setIdArea(AreasDTO idArea) {
        this.idArea = idArea;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }


    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
}
