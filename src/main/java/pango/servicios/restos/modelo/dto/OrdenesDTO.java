/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.modelo.dto;

import java.sql.Timestamp;

/**
 *
 * @author in10c
 */

public class OrdenesDTO {
    private Long id;
    private MesasDTO idMesa;
    private ClientesDTO idCliente;
    private UsuariosDTO idUsuario;
    private Integer estado;
    private Timestamp tiempoRegistro;
    
    public OrdenesDTO (MesasDTO idMesa, ClientesDTO idCliente, UsuariosDTO idUsuario){
        this.idMesa = idMesa;
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.estado=1;
    }
    
    public OrdenesDTO (){
        
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MesasDTO getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(MesasDTO idMesa) {
        this.idMesa = idMesa;
    }

    public ClientesDTO getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(ClientesDTO idCliente) {
        this.idCliente = idCliente;
    }

    public UsuariosDTO getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UsuariosDTO idUsuario) {
        this.idUsuario = idUsuario;
    }


    public Timestamp getTiempoRegistro() {
        return tiempoRegistro;
    }

    public void setTiempoRegistro(Timestamp tiempoRegistro) {
        this.tiempoRegistro = tiempoRegistro;
    }
    
    
    
}
