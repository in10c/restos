/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.modelo.dao.impl;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import pango.servicios.restos.modelo.dao.MaestroDAO;
import pango.servicios.restos.modelo.dto.*;

/**
 *
 * @author in10c
 */
public class MaestrosDAOImpl  extends DaoSupport implements MaestroDAO {

    public void registrarObjeto(Object cosa) throws Exception {
        Session session = null;      //conexi??n a la base de datos   
        session = this.getSession(); //establecer comunicaci??n    
        System.out.println("llego hasta la implementaci??n :D");
        try {
            if (cosa instanceof MesasDTO){
                System.out.println("Objeto instancia de mesas, se guardara...");
                MesasDTO mesa = (MesasDTO)cosa; 
                save(mesa);  //insert into *
            }
            else if (cosa instanceof UsuariosDTO){
                System.out.println("Objeto instancia de usuarios, se guardara...");
                UsuariosDTO usu = (UsuariosDTO)cosa; 
                save(usu);  //insert into *
            }
            else if (cosa instanceof ClientesDTO){
                System.out.println("Objeto instancia de clientes, se guardara...");
                ClientesDTO cli = (ClientesDTO)cosa; 
                save(cli);  //insert into *
            }
            else if (cosa instanceof ProductosDTO){
                System.out.println("Objeto instancia de productos, se guardara...");
                ProductosDTO prod = (ProductosDTO)cosa; 
                save(prod);  //insert into *
            }
            
            else if (cosa instanceof CategoriasDTO){
                System.out.println("Objeto instancia de categorias, se guardara...");
                CategoriasDTO cate = (CategoriasDTO)cosa; 
                save(cate);  //insert into *
            }
            
            else if (cosa instanceof AreasDTO){
                System.out.println("Objeto instancia de areas, se guardara...");
                AreasDTO area = (AreasDTO)cosa; 
                save(area);  //insert into *
            }
            
            else if (cosa instanceof OrdenesDTO){
                System.out.println("Objeto instancia de ordenes, se guardara...");
                OrdenesDTO orden = (OrdenesDTO)cosa; 
                save(orden);  //insert into *
            }
            
            else if (cosa instanceof DetallesOrdenesDTO){
                System.out.println("Objeto instancia de detalles ordenes, se guardara...");
                DetallesOrdenesDTO detOrden = (DetallesOrdenesDTO)cosa; 
                save(detOrden);  //insert into *
            }
            
        } catch (Exception e) {
            System.out.println("Error de inserci??n "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session); // cerrar sesi??n
        }
    }

    public List<Object> consultarObjetos(String tipo) throws Exception {
        Session session = null;      //conexi??n a la base de datos   
        session = this.getSession(); //establecer comunicaci??n          
        try {
            if(tipo=="clientes"){
                Criteria criteria = session.createCriteria(ClientesDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="mesas"){
                Criteria criteria = session.createCriteria(MesasDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="usuarios"){
                Criteria criteria = session.createCriteria(UsuariosDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="productos"){
                System.out.println("se estan consultando productos");
                Criteria criteria = session.createCriteria(ProductosDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="categorias"){
                System.out.println("se estan consultando categorias");
                Criteria criteria = session.createCriteria(CategoriasDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="areas"){
                System.out.println("se estan consultando areas");
                Criteria criteria = session.createCriteria(AreasDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="ordenes"){
                System.out.println("se estan consultando ordenes");
                Criteria criteria = session.createCriteria(OrdenesDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.addOrder(Property.forName("id").asc()); //order by que ordene por nombre y asc o descendente
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                System.out.println("regreso un total de "+criteria.list().size());
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
            else if(tipo=="detallesordenes"){
                System.out.println("se estan consultando detalles ordenes");
                Criteria criteria = session.createCriteria(DetallesOrdenesDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                System.out.println("regreso un total de "+criteria.list().size());
                
                return (List<Object>)criteria.list(); //saca datos y hace cast para con lista de alumnos
            }
            
        } catch (Exception e) {
            System.out.println("Error de consulta "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session); // cerrar sesi??n
        }
        return null;
    }

    public Object buscarObjeto(Long id, String tipo) throws Exception {
        Session session = null;        
        session = this.getSession();           
        try {
            if (tipo=="clientes"){
                Criteria criteria = session.createCriteria(ClientesDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (ClientesDTO)criteria.uniqueResult();           
            }
            
            else if (tipo=="mesas"){
                Criteria criteria = session.createCriteria(MesasDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (MesasDTO)criteria.uniqueResult();           
            }
            
            else if (tipo=="clientes"){
                Criteria criteria = session.createCriteria(ClientesDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (ClientesDTO)criteria.uniqueResult();           
            }
            
            else if (tipo=="productos"){
                Criteria criteria = session.createCriteria(ProductosDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (ProductosDTO)criteria.uniqueResult();           
            }
            else if (tipo=="usuarios"){
                Criteria criteria = session.createCriteria(UsuariosDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (UsuariosDTO)criteria.uniqueResult();           
            }
            else if (tipo=="ordenes"){
                Criteria criteria = session.createCriteria(OrdenesDTO.class);            
                criteria.add(Restrictions.eq("id", id));
                return (OrdenesDTO)criteria.uniqueResult();           
            }
            
        } catch (Exception e) {
            System.out.println("Error de consulta "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session);
        }
        return null;
    }

    public void actualizarObjeto(Object cosa) throws Exception {
        Session session = null;      //conexi??n a la base de datos   
        session = this.getSession(); //establecer comunicaci??n    
        try { 
            if (cosa instanceof MesasDTO){
                System.out.println("Objeto instancia de mesas, se guardara...");
                MesasDTO mesa = (MesasDTO)cosa; 
                this.saveOrUpdate(mesa);
            }
            else if (cosa instanceof UsuariosDTO){
                System.out.println("Objeto instancia de usuarios, se guardara...");
                UsuariosDTO usu = (UsuariosDTO)cosa; 
                this.saveOrUpdate(usu);
            }
            else if (cosa instanceof ClientesDTO){
                System.out.println("Objeto instancia de clientes, se guardara...");
                ClientesDTO cli = (ClientesDTO)cosa; 
                this.saveOrUpdate(cli);
            }
            else if (cosa instanceof ProductosDTO){
                System.out.println("Objeto instancia de productos, se guardara...");
                ProductosDTO prod = (ProductosDTO)cosa; 
                this.saveOrUpdate(prod);
            }
        } catch (Exception e) {
            System.out.println("Error de actualizaci??n "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session); // cerrar sesi??n
        }
    }

    public void eliminarObjeto(Long id, String tipo) throws Exception {
        Session session = null;
        session = this.getSession();
        SQLQuery query;
        try {
                query = session.createSQLQuery("UPDATE "+tipo+" set estado=0 WHERE id = :id ");
                query.setParameter("id", id);
                      
            if (query.executeUpdate() != 1) {
                throw new Exception();
            }
            this.releaseSession(session);        
        } catch (HibernateException e) {
            System.out.println("Error de eliminar.-  "+e.getMessage());            
        }  
    }

    public UsuariosDTO validarUsuario(String usuario) throws Exception {
        Session session = null;
        session = this.getSession();
        try {
            Criteria criteria = session.createCriteria(UsuariosDTO.class);
            criteria.add(Restrictions.eq("usuario", usuario));
            criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
            return (UsuariosDTO)criteria.uniqueResult();
        } catch (Exception e) {
            throw e;
        } finally {
            this.releaseSession(session);
        }
    }

    public List<ProductosDTO> consultarProductosFiltrados(Long idCategoria, Integer tipoMenu) throws Exception {
        System.out.println("llego a consutar productos filtrados");
        Session session = null;      //conexi??n a la base de datos   
        session = this.getSession(); //establecer comunicaci??n          
        try {
                Criteria criteria = session.createCriteria(ProductosDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.add(Restrictions.eq("estado", 1)); //semestre si fuera = a 1 como un where
                if(idCategoria!=0){
                    criteria.add(Restrictions.eq("idCategoria.id", idCategoria));
                }
                criteria.add(Restrictions.eq("tipoMenu", tipoMenu));
                criteria.addOrder(Property.forName("nombre").asc()); //order by que ordene por nombre y asc o descendente
                return (List<ProductosDTO>)criteria.list(); //saca datos y hace cast para con lista de alumnos 
        } catch (Exception e) {
            System.out.println("Error de consulta "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session); // cerrar sesi??n
        }
    }

    public List<DetallesOrdenesDTO> consulDetallesFiltrados(Long id) throws Exception {
        Session session = null;      //conexi??n a la base de datos   
        session = this.getSession(); //establecer comunicaci??n          
        System.out.println("ando en el dao impl");
        try {
                Criteria criteria = session.createCriteria(DetallesOrdenesDTO.class); //esa clase hace referencia a la tabla de la bd SELECT * FROM alumnos
                criteria.add(Restrictions.eq("idOrden.id", id));
                return (List<DetallesOrdenesDTO>)criteria.list(); //saca datos y hace cast para con lista de alumnos 
        } catch (Exception e) {
            System.out.println("Error de consulta "+e.getMessage());
            throw e;
        } finally {
            this.releaseSession(session); // cerrar sesi??n
        }
    }
    
}
