/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.servicio;

import java.util.List;
import pango.servicios.restos.modelo.dto.DetallesOrdenesDTO;
import pango.servicios.restos.modelo.dto.ProductosDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;

/**
 *
 * @author in10c
 */
public interface ServicioMaestro {
    public void registrarObjeto(Object cosa) throws Exception;
    public List<Object> consultarObjetos(String tipo) throws Exception;
    public Object buscarObjeto(Long id, String tipo) throws Exception;
    public void actualizarObjeto(Object cosa) throws Exception;
    public void eliminarObjeto(Long id, String tipo) throws Exception;
    public UsuariosDTO autenticarUsuario(UsuariosDTO usuario) throws Exception;
    public void cerrarSesion(UsuariosDTO usuario) throws Exception;

    public List<ProductosDTO> consultarProductosFiltrados(Long idCategoria, Integer tipoMenu) throws Exception;

    public List<DetallesOrdenesDTO> consulDetallesFiltrados(Long id) throws Exception;
}
