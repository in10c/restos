/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.servicio.impl;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import pango.servicios.restos.modelo.dao.MaestroDAO;
import pango.servicios.restos.modelo.dto.DetallesOrdenesDTO;
import pango.servicios.restos.modelo.dto.ProductosDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;
import pango.servicios.restos.servicio.ServicioMaestro;

/**
 *
 * @author in10c
 */
public class ServiciosMaestrosImpl extends ActionSupport implements ServicioMaestro {
    private MaestroDAO maestrodao;

    public MaestroDAO getMaestrodao() {
        return maestrodao;
    }

    public void setMaestrodao(MaestroDAO maestrodao) {
        this.maestrodao = maestrodao;
    }

    @Override
    @Transactional
    public void registrarObjeto(Object cosa) throws Exception {
        maestrodao.registrarObjeto(cosa);
    }

    @Transactional
    public List<Object> consultarObjetos(String tipo) throws Exception {
        return maestrodao.consultarObjetos(tipo);
    }

    @Transactional
    public Object buscarObjeto(Long id, String tipo) throws Exception {
        return maestrodao.buscarObjeto(id, tipo);
    }

    @Override
    @Transactional
    public void actualizarObjeto(Object cosa) throws Exception {
        maestrodao.actualizarObjeto(cosa);
    }

    @Override
    @Transactional
    public void eliminarObjeto(Long id, String tipo) throws Exception {
        maestrodao.eliminarObjeto(id, tipo);
    }

    @Transactional
    public UsuariosDTO autenticarUsuario(UsuariosDTO usuario) throws Exception {
        UsuariosDTO result = maestrodao.validarUsuario(usuario.getUsuario());
        if(result != null){
            if(result.getContrasena().equals(usuario.getContrasena())){
                return result;
            }else{
                this.addActionError("Nombre de Usuario o Password Incorrectos");
                return null;
            }
        }else{
            return null;
        }
    }

    @Transactional
    public void cerrarSesion(UsuariosDTO usuario) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    public List<ProductosDTO> consultarProductosFiltrados(Long idCategoria, Integer tipoMenu)  throws Exception{
        System.out.println("llego al servicio impl");
        return maestrodao.consultarProductosFiltrados(idCategoria, tipoMenu);
    }
    
    @Transactional
    public List<DetallesOrdenesDTO> consulDetallesFiltrados(Long id) throws Exception {
        return maestrodao.consulDetallesFiltrados(id);
    }
    
}
