/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.utilerias;

/**
 *
 * @author luis
 */
public class Loader {
    private String version;

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param aVersion the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

}
