/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pango.servicios.restos.utilerias;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import pango.servicios.restos.modelo.dto.AreasDTO;
import pango.servicios.restos.modelo.dto.CategoriasDTO;
import pango.servicios.restos.modelo.dto.UsuariosDTO;

/**
 *
 * @author in10c
 */
public class Navegacion extends ActionSupport implements SessionAware{
    private String datos;
    private Map session;
    private UsuariosDTO usuario_sesion;



    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    public UsuariosDTO getUsuario_sesion() {
        return usuario_sesion;
    }

    public void setUsuario_sesion(UsuariosDTO usuario_sesion) {
        this.usuario_sesion = usuario_sesion;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
   
    
    public String enlaceRegClientes(){
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        datos = "clientes";
        return SUCCESS;
    }
    
    public String irMenu(){
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        return SUCCESS;
    }
    
    public String acceder(){
        
        return SUCCESS;
    }
    
     public String enlaceRegMesas(){
         // se leen las variables de session  
        //no verificamos si es nula o no, simplemente la usamos, el interceptor se encarga de la validación
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        
        datos = "mesas";
        return SUCCESS;
    }
    
    public String formRegUsuarios(){
        usuario_sesion = (UsuariosDTO) this.session.get("usuario");
        datos = "usuarios";
        return SUCCESS;
    }
    
   

   
}
