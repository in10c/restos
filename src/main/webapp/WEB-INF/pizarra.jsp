<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />

<div>
	<div id="parteAlta">
		<div class="contenedorCate">
			<div id="ruta">
				<div class="cambiador">
					<a href="irMenu.action">
						<span style="text-align:center;">
							<img src="<%=request.getContextPath()%>/imagenes/home.png" width="40" height="40">
						</span>
					</a>
				</div>
				<div class="cambiador">
					<a href="#" id="tipoMenuSeleccionado" data-tipo="1" onclick="abrirSelectorMenu()">
						<span>
							Tipo de menu
						</span>
						<span>
							<img src="<%=request.getContextPath()%>/imagenes/abajo.png" width="40" height="40">
						</span>	
					</a>	
				</div>
				<div class="cambiador">
					<a href="#" id="tipoCateSeleccionada" data-categoria="0" onclick="abrirSelectorCategorias()">
						<span>
							Categorias 
						</span>
						<span>
							<img src="<%=request.getContextPath()%>/imagenes/abajo.png" width="40" height="40">
						</span>	
					</a>	
				</div>
			</div>
			<div id="contenedorProductos">
                                
				<s:iterator value="listaProductos" status="status"> 
                                        <div id="prod${status.count}" data-id="${id}" data-imagen="${imagen}" data-nombre="${nombre}" data-contador="${status.count}"  data-precio="${precio}"  class="producto" style="background: url('<%=request.getContextPath()%>/subidas/fotosProd/${imagen}'); background-size: 100% 100%;">
                                            
                                            <div class="legenda">
                                                <span class="tituloProd">${nombre}</span>
                                            </div>
                                            
                                            <div id="wrap${status.count}" class="wrapBotones">
                                                <a onclick="agregarAOrden(${status.count})" href="#"><i class="fa fa-cutlery"></i></a>
                                                <a href="#"><i class="fa fa-info-circle"></i></a>
                                            </div>    
                                                
                                        </div>
                                        
                                </s:iterator>
			</div>
		</div>
		<div id="contenedorCuenta">
                    <h2>Elementos de la orden</h2>
                    <table id="elemCuentaTabla">
                        <thead>
                            <tr>
                                <th>Selecci�n</th>
                                <th>Unitario</th>
                                <th>Cant.</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    <div id="Totales">
                        <p>
                            <span>
                                Subtotal:
                            </span> 
                            <span id="cantSubtotal">
                                0 MXN
                            </span>
                        </p>
                        <p>
                            <span>
                                Total:
                            </span> 
                            <span id="cantTotal">
                                0 MXN
                            </span>
                        </p>
                    </div>
		</div>
	</div>
	<div id="parteBaja">
		<div id="enlaceMesas">
			<a href="#" onclick="abrirSelMesas()"><span style="text-align:right;"><img src="<%=request.getContextPath()%>/imagenes/mesaRest.png" width="80"></span>
			<span data-id="0" id="mesaSeleccionada" class="tituloMesa">Seleccion mesa</span></a>
		</div>
		<div id="clientesArea">
			<p style="margin-top:2%;">
                            <a onclick="abrirSelecCli()" href="#"><span class="subtitulo">Cliente: </span><br>
                                <span id="clienteSeleccionado" data-id="0" data-idUser="${usuario_sesion.id}">P?blico en general</span></a>
			</p>
		</div>
		<div>
			<p style="margin-top:4%; margin-left:30%;">
				<a class="botonBonito" id="enviarOrden" href="#">Enviar orden</a>
			</p>
		</div>
	</div>
</div>

<div id="selectorMesas" class="flotante">
	<h2>Selector de mesas <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
	Porfavor seleccione la mesa de la cual tomara la orden.
	<div id="contenedorMesas">
		<ul class="listaFancy">
			<s:iterator value="listaMesas"> 
	            <li><a onclick="aplicarMesa(${id}, '${nombre}')" href="#">${nombre}</a></li>
            </s:iterator>
		</ul>
	</div>
</div>

<div id="selectorMenu" class="flotante">
	<h2>Selector de menu <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
	Porfavor seleccione el tipo de menu activo.
	<div id="listaTiposMenu">
		<p>
			<input type="radio" checked="checked"  name="tipoMenu" value="1"> Desayuno
		</p>
		<p>
			<input type="radio"  name="tipoMenu"  value="2"> Comida
		</p>
		<p>
			<input type="radio" name="tipoMenu" value="3" > Cena
		</p>
	</div>
</div> 

<div id="selectorCategorias" class="flotante">
	<h2>Selector de categorias <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
	<div>
		<ul id="listaCatego" class="listaFancy">
			<s:iterator value="listaCategorias" status="status"> 
                            <li><a onclick="aplicarCategoria(${id}, ${status.count})" href="#">${nombre}</a></li>
                        </s:iterator>
		</ul>
	</div>
</div>
                        
<div id="selectorClientes" class="flotante">
	<h2>Selector de clientes <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
	<div>
		<ul id="listaCli" class="listaFancy">
			<s:iterator value="listaClientes"> 
                            <li><a data-id="${id}" onclick="aplicarCliente( ${id},'${nombre} ${apellidoPaterno}')" href="#">${nombre} ${apellidoPaterno} ${apellidoMaterno}</a></li>
                        </s:iterator>
		</ul>
	</div>
</div>                        
                        
<div id="ModCantidades" data-trabajando="0" class="flotante" style="height:150px; left: 44%; width: 20%;">
     
</div>   
                        
<div id="alerta" class="flotante" style="height:180px; left: 35%; top:30%;width: 30%;overflow-y: auto;">
    <h2><span class="modTitu">Titulo </span> <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
    <span class="modCont">
        
    </span>    
</div>                           

<jsp:include page="pie.jsp" />        

<script type="text/javascript">
	$(document).on("ready", iniciar);
	//listaClientes
	function iniciar () {
		$('input:radio').on("change",aplicarTipoMenu);
                $(".producto").on("click", ponerBotones);
                $("#enviarOrden").on("click", enviarOrden);
	}

	function aplicarTipoMenu(){
		$("#tipoMenuSeleccionado").attr("data-tipo", $(this).val()); 
                objCategoria = $("#tipoCateSeleccionada");
                var idCate = parseInt(objCategoria.attr("data-categoria"));
                var tipoMenu = parseInt($(this).val());
                $.ajax({
                    type: "POST",   
                    dataType: "json",
                    url:"buscarProductosFiltrados.action",
                    data:"idCategoria="+idCate+"&tipoMenu="+tipoMenu,
                    async: false,
                    beforeSend: function() {
                    },
                    success: function(data) {
                       $("#contenedorProductos").html(data.salida);
                       $(".producto").on("click", ponerBotones);
                    },
                    error: function(jqXHR, error, errorThrown) {
                        ok=0;
                    }
                });
	}
        
        function aplicarCategoria(idCat, posicion){
                objCategoria = $("#tipoCateSeleccionada");
                objCategoria.attr("data-categoria", idCat); 
                objMenu = $("#tipoMenuSeleccionado");
                var tipoMenu = parseInt(objMenu.attr("data-tipo"));
                $.ajax({
                    type: "POST",   
                    dataType: "json",
                    url:"buscarProductosFiltrados.action",
                    data:"idCategoria="+idCat+"&tipoMenu="+tipoMenu,
                    async: false,
                    beforeSend: function() {
                    },
                    success: function(data) {
                       $("#contenedorProductos").html(data.salida);
                       $(".producto").on("click", ponerBotones);
                       $("#listaCatego li").each(function(){
                           $(this).removeClass("categSelec");
                           $(this).find("a").css("color", "#7E397A");
                       });
                       var newpos = (parseInt(posicion)-1);
                       $("#listaCatego li").eq(newpos).addClass("categSelec");
                       $("#listaCatego li").eq(newpos).find("a").css("color", "white");
                    },
                    error: function(jqXHR, error, errorThrown) {
                        ok=0;
                    }
                });
        }
        
        function aplicarCliente (id, nombre){
            obj = $("#clienteSeleccionado");
            obj.text(nombre);
            obj.attr("data-id", id);
        }
        
        function enviarOrden(){
            var idUsu = $("#clienteSeleccionado").attr("data-idUser");
            var idCli = $("#clienteSeleccionado").attr("data-id");
            var idMes = parseInt($("#mesaSeleccionada").attr("data-id"));
            var numProd = contarPlatillos(); 
            if(idMes==0){
                alertar("Sin mesa seleccionada", "�Atenci?n!, debes seleccionar una mesa para proceder con la orden.");
            }
            else if (numProd==0) {
                alertar("Sin platillos seleccionados","�Atenci?n!, debes seleccionar al menos un platillo para proceder con la orden." );
            }
            else {
                $.ajax({
                    type: "POST",   
                    dataType: "json",
                    url:"guardarOrden.action",
                    data:"idMesa="+idMes+"&idUsuario="+idUsu+"&idCliente="+idCli,
                    async: false,
                    beforeSend: function() {
                    },
                    success: function(data) {
                       guardarElementos(data.orden.id);
                    },
                    error: function(jqXHR, error, errorThrown) {
                        ok=0;
                        alertar("Error", "Ocurri? un error en la petici?n, prueba a hacer el pedido de nuevo.");
                    }
                });
            }
        }
        
        function guardarElementos(idOrden){
            var num = contarPlatillos();
            var contador = 0;
            $("#elemCuentaTabla tr.elemento").each(function(){
                var cantidad = parseInt($(this).find("td.cantidades").text());
                var precUnit = parseInt($(this).find("td.precios").text());
                var precTotal = parseInt($(this).find("td.totales").text());
                var idProd = parseInt($(this).attr("data-id"));
                //alert("id="+id+"  cant="+cantidad+" unit="+precUnit+" tot="+precTotal);
                $.ajax({
                    type: "POST",   
                    dataType: "json",
                    url:"guardarDetalle.action",
                    data:"idOrden="+idOrden+"&idProd="+idProd+"&cantidad="+cantidad+"&precUnit="+precUnit+"&precTotal="+precTotal,
                    async: false,
                    beforeSend: function() {
                    },
                    success: function(data) {
                       contador+=1;
                       if(contador==num){
                           $("#enviarOrden").off("click");
                           alertar("�?xito!", "La orden fu? enviada satisfactoriamente.<br><br><a class='botonBonito' style='float:right;' onclick='location.reload();' href='#'>Nueva orden</a>");
                       }
                    },
                    error: function(jqXHR, error, errorThrown) {
                        ok=0;
                        alertar("Error", "Ocurri? un error en la petici?n, prueba a hacer el pedido de nuevo.");
                    }
                });
            });
        }
        
        function alertar(titulo, cuerpo){
            alerta = $("#alerta");
            alerta.find(".modTitu").text(titulo);
            alerta.find(".modCont").html(cuerpo);
            alerta.show();
        }
        
        function contarPlatillos(){
            var i =0;
            $("#elemCuentaTabla tr.elemento").each(function(){
                    i += 1; 
              });
            return i;
        }
        
</script>

