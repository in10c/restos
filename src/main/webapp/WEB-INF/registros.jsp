
<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />



<s:div cssClass="contenedor">
    
<s:if test="usuario_sesion.tipo==1">
    
        <s:if test="%{datos=='mesas'}">
           <h1>Registro de mesas</h1>
           <s:form name="FormuMesa" action="registrarMesa" method="post" >
               <s:textfield label="Nombre" onKeyUp='this.value=this.value.toUpperCase()'
                                     required='true' name="mesa.nombre" />
               <s:textfield label="Capacidad" required="true" type="number" min="0" name="mesa.capacidad" />
               <s:reset value="Limpiar datos" /> <s:submit value="Guardar mesa" />
           </s:form>
        </s:if> 
           
        <s:elseif test="%{datos=='clientes'}">
            <h1>Registro de clientes</h1>
           <s:form name="FormuClien" action="registrarCliente" method="post" >
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true'  label="Nombre" name="clien.nombre" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Paterno" name="clien.apellidoPaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Materno" name="clien.apellidoMaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true'  label="Direcci�n" name="clien.direccion" />
               <s:textfield type="email" required='true'  label="Correo Electr�nico" name="clien.correoElectronico" />
               <s:reset value="Limpiar datos" /> <s:submit value="Guardar cliente" />
           </s:form>   
        </s:elseif>
           
        <s:elseif test="%{datos=='usuarios'}">
            <h1>Registro de usuarios</h1>
            <s:form name="FormuUsu" action="registrarUsuarios" method="post">
               
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre de usuario" name="usu.usuario" />
               <s:textfield type="password" required='true' label="Contrase�a" name="usu.contrasena" />
               <s:select label="Tipo de usuario" required='true' name="usu.tipo" 
                        headerKey="-1" headerValue="--Seleccione tipo de usuario--"
                        list="#@java.util.LinkedHashMap@{'1':'Administrador',
                                                         '2':'Empleado'}"  />
             
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre" name="usu.nombre" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Paterno" name="usu.apellidoPaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Materno" name="usu.apellidoMaterno" />
               <s:textarea onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Direcci�n"  rows="5" name="usu.direccion" />
               
               
              <s:reset value="Limpiar datos" /> <s:submit value="Guardar usuario" />
           </s:form>  
        </s:elseif>
           
        <s:elseif test="%{datos=='prod'}">
            <h1>Registro de producto</h1>
            <s:a cssClass="botonBonito" onclick="abrirAdminCat()" href="#">Administrar categorias</s:a>
               <s:a cssClass="botonBonito" onclick="abrirAdminArea()" href="#">Administrar �reas</s:a>
               
               <br>
           <s:form name="FormuProd" action="registrarProd" enctype="multipart/form-data" method="post" >
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre" name="prod.nombre" />
               <s:textarea onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Descripci�n" name="prod.descripcion" cols="50" rows="7" />
               <s:file label="Imagen" name="imagen" />
               <s:textfield required='true' type="number" min="0" label="Precio" name="prod.precio" />
               
               <s:select required='true' label="Tipo de men�" name="prod.tipoMenu" 
                        headerKey="-1" headerValue="--Seleccione tipo de men�--"
                        list="#@java.util.LinkedHashMap@{'1':'Desayuno',
                                                         '2':'Comida',
                                                         '3':'Cena'}"  />
               
               <s:div id="contieneComboCat">
               <s:select
                   required='true'
                    list="listaCategorias"
                    name="prod.idCategoria.id"
                    listKey="id"
                    listValue="%{nombre}"
                    label="Categoria"
                />
               </s:div> 
               
               <s:select
                   required='true'
                    list="listaAreas"
                    name="prod.idArea.id"
                    listKey="id"
                    listValue="%{nombre}"
                    label="�rea"
                />
               
               <s:reset value="Limpiar datos" /> <s:submit value="Guardar producto" />
           </s:form>     
               
               <s:div id="adminCat" cssClass="flotante">
                   <h2>Administraci�n de categor�as<a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>   
                   Atenci�n. Al borrar una categor�a los productos pertenecientes a ella misma ya no podr�n ser a�adidos a una orden.<br>
                   <div style="margin-top: 8px;" id="contieneTablaCat">
                   <table class="tablaGrande invertirColor" >
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator value="listaCategorias"> <!--esto corresponde al atributo DTO-->
                        <tr>
                            <td>${nombre} </td>
                            <td>
                                <a class="botonBonito" onclick="eliminarCategoria(${id})" href="#">Eliminar</a>    
                            </td>
                        </tr>
                        </s:iterator>
                    </tbody>
                </table>
                       </div>
                   <br>
                   <s:textfield id="nombreNuevaCat" label="Nueva categor�a" /> 
                   <div style="float: right; margin-top: 5px;">
                   <s:a cssClass="botonBonito" onclick="agregarCat()" href="#">Agregar categor�a</s:a>
                   </div>
                   
               </s:div>    
                   
               <s:div id="adminArea" cssClass="flotante">
                   <h2>Administraci�n de �reas<a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
                   <div style="margin-top: 8px;" id="contieneTablaArea">
                   <table class="tablaGrande invertirColor" >
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator value="listaAreas"> <!--esto corresponde al atributo DTO-->
                        <tr>
                            <td>${nombre} </td>
                            <td>
                                <a class="botonBonito" onclick="eliminarArea(${id})" href="#">Eliminar</a>    
                            </td>
                        </tr>
                        </s:iterator>
                    </tbody>
                </table>
                       </div>
                   <br>
                   <s:textfield id="nombreNuevaArea" label="Nueva �rea" /> 
                   <div style="float: right; margin-top: 5px;">
                   <s:a cssClass="botonBonito" onclick="agregarArea()" href="#">Agregar �rea</s:a>
                   </div>
                   
               </s:div>        
             
        </s:elseif>   
           
        
 </s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>
        <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 
   </s:div>     
<jsp:include page="pie.jsp" />
