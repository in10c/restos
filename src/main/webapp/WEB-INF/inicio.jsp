<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="menu">
    <h2>Bienvenido ${usuario_sesion.nombre} </h2>
    <ul>
        <s:if test="usuario_sesion.tipo==1">
            <li><s:a href="formRegMesas.action">Registrar mesas</s:a></li>
            <li><s:a href="verRegMesas.action">Ver mesas</s:a></li>
            <hr>
            <li><s:a href="formRegClientes.action">Registrar clientes</s:a></li>
            <li><s:a href="consultaClientes.action">Ver clientes</s:a></li>
            <hr>
            <li><s:a href="formRegUsuarios.action">Registrar usuarios</s:a></li>
            <li><s:a href="consultaUsuarios.action">Ver usuarios</s:a></li>
            <hr>
            <li><s:a href="formRegProd.action">Registrar platillos</s:a></li>
            <li><s:a href="consultaProd.action">Ver platillos</s:a></li>
            <hr>
        </s:if>
        <li><s:a href="pizarraPedidos.action">Pizarra de pedidos</s:a></li>
        <li><s:a href="pizarraCocina.action">Pizarra de cocina</s:a></li>
        <hr>
        <li><s:a href="cerrarsesion.action">Cerrar sesi�n</s:a></li>
    </ul>
</s:div>
        
        
<jsp:include page="pie.jsp" />    
