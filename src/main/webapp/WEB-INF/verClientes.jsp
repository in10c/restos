<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
    <s:if test="usuario_sesion.tipo==1">
        <h1>Lista de clientes</h1>
        
        <s:if test="clientes.size>0">
            <table class="tablaGrande">
                <thead>
                    <tr>
                        <th>NOMBRE COMPLETO</th>
                        <th>DIRECCI�N</th>
                        <th>CORREO ELECTR�NICO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="clientes"> <!--esto corresponde al atributo DTO-->
                    <tr>
                        <th>${nombre} ${apellidoPaterno} ${apellidoMaterno}</th>
                        <th>${direccion}</th>
                        <th>${correoElectronico}</th>
                        <th>
                        <s:a cssClass="botonBonito" href="editarCliente.action?id=%{id}">Editar</s:a>
                        <s:a cssClass="botonBonito" onclick="javascript:return confirm('�Est�s seguro de querer eliminar este registro?');" href="eliminarCliente.action?id=%{id}">Eliminar</s:a>    
                        </th>
                    </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:if>
        </s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>
    <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 
        </s:div>     
<jsp:include page="pie.jsp" />  
