<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
    <s:if test="usuario_sesion.tipo==1">
        <h1>Lista de mesas</h1>
        
        <s:if test="mesas.size>0">
            <table id="listaCategoriasAdmin" class="tablaGrande" >
                <thead>
                    <tr>
                        <th>NOMBRE</th>
                        <th>CAPACIDAD</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="mesas"> <!--esto corresponde al atributo DTO-->
                    <tr>
                        <th>${nombre} </th>
                        <th>${capacidad}</th>
                        <th>
                            <s:a cssClass="botonBonito" href="editarMesa.action?id=%{id}">Editar</s:a>
                            <s:a cssClass="botonBonito" onclick="javascript:return confirm('�Est�s seguro de querer eliminar este registro?');" href="eliminarMesa.action?id=%{id}">Eliminar</s:a>    
                        </th>
                    </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:if>
</s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>
        <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 
        
</s:div>     
<jsp:include page="pie.jsp" />