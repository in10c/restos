<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
        <h1>!�xito!</h1>
        <s:property value="salida"/>
        <br>
        <div class="menuRetorno">
            <s:a href="irMenu.action">Regresar al men� principal</s:a> 
            <s:if test="%{datos=='productos'}">
                <s:a href="consultaProd.action">Ir al administrador de platillos</s:a> 
                <s:a href="formRegProd.action">Registrar nuevo platillo</s:a> 
            </s:if> 
            <s:if test="%{datos=='mesas'}">
                <s:a  href="verRegMesas.action">Ir al administrador de mesas</s:a> 
                <s:a  href="formRegMesas.action">Registrar nueva mesa</s:a> 
            </s:if>  
            <s:if test="%{datos=='clientes'}">
                <s:a  href="consultaClientes.action">Ir al administrador de clientes</s:a> 
                <s:a  href="formRegClientes.action">Registrar nuevo cliente</s:a> 
            </s:if> 
            <s:if test="%{datos=='usuarios'}">
                <s:a  href="consultaUsuarios.action">Ir al administrador de usuarios</s:a> 
                <s:a  href="formRegUsuarios.action">Registrar nuevo usuario</s:a> 
            </s:if>     
        </div>    
</s:div>     
<jsp:include page="pie.jsp" />        
