<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
    <s:if test="usuario_sesion.tipo==1">
        <h1>Lista de usuarios</h1>
      
        <s:if test="usuarios.size>0">
            <table class="tablaGrande">
                <thead>
                    <tr>
                        <th>USUARIO</th>
                        <th>NOMBRE COMPLETO</th>
                        <th>TIPO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="usuarios"> <!--esto corresponde al atributo DTO-->
                    <tr>
                        <th>${usuario} </th>
                        <th>${nombre} ${apellidoPaterno} ${apellidoMaterno}</th>
                        <th>
                            <s:if test="tipo==1">
                                Administrador
                            </s:if>
                            <s:else>
                                Empleado
                            </s:else>
                        </th>
                        <th>
                        <s:a cssClass="botonBonito" href="editarUsuario.action?id=%{id}">Editar</s:a>
                        <s:a cssClass="botonBonito" onclick="javascript:return confirm('�Est�s seguro de querer eliminar este registro?');" href="eliminarUsuario.action?id=%{id}">Eliminar</s:a>    
                        </th>
                    </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:if>
   </s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>     
    <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 
        </s:div>     
<jsp:include page="pie.jsp" />  

