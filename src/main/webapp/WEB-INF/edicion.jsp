<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
   <s:if test="usuario_sesion.tipo==1">     
        <s:if test="%{datos=='mesas'}">
            <h1>Edici�n de mesas</h1>
            <s:form name="FormuMesaAct" action="guardarCambiosMesa" method="post" >
               <s:hidden name="mesa.id" value="%{clonMesa.id}" />
               <s:textfield label="Nombre" name="mesa.nombre" onKeyUp='this.value=this.value.toUpperCase()' required='true' value="%{clonMesa.nombre}"/>
               <s:textfield label="Capacidad" name="mesa.capacidad" required="true" type="number" min="0" value="%{clonMesa.capacidad}"/>
               <s:hidden name="mesa.estado" value="%{clonMesa.estado}" />
               <s:submit value="Actualizar mesa" />
            </s:form>
        </s:if>
        
        <s:elseif test="%{datos=='clientes'}">
            <h1>Edici�n de clientes</h1>
             <s:form name="FormuClienAct" action="guardarCambiosCliente" method="post" >
                 <s:hidden name="clien.id" value="%{clonClien.id}" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true'  label="Nombre" value="%{clonClien.nombre}" name="clien.nombre" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Paterno" value="%{clonClien.apellidoPaterno}" name="clien.apellidoPaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Materno" value="%{clonClien.apellidoMaterno}" name="clien.apellidoMaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Direcci�n" value="%{clonClien.direccion}" name="clien.direccion" />
               <s:textfield  required='true' type="email" label="Correo Electr�nico" value="%{clonClien.correoElectronico}" name="clien.correoElectronico" />
               
               <s:hidden name="clien.estado" value="%{clonClien.estado}" />
               <s:submit value="Actualizar cliente" />
           </s:form>    
        </s:elseif>
        
        <s:elseif test="%{datos=='usuarios'}">
            <h1>Edici�n de usuarios</h1>
             <s:form name="FormuUsuAct" action="guardarCambiosUsuario" method="post" >
                 <s:hidden name="usu.id" value="%{clonUsu.id}" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre de usuario" value="%{clonUsu.usuario}" name="usu.usuario" />
               <s:textfield type="password" required='true' label="Contrase�a"  value="%{clonUsu.contrasena}" name="usu.contrasena" />
               <s:select label="Tipo de usuario" required='true' name="usu.tipo" 
                        headerKey="%{clonUsu.tipo}" 
                        value="%{clonUsu.tipo}"
                        list="#@java.util.LinkedHashMap@{'1':'Administrador',
                                                         '2':'Empleado'}"  />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre" value="%{clonUsu.nombre}" name="usu.nombre" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Paterno" value="%{clonUsu.apellidoPaterno}" name="usu.apellidoPaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Apellido Materno" value="%{clonUsu.apellidoMaterno}" name="usu.apellidoMaterno" />
               <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Direcci�n" value="%{clonUsu.direccion}" name="usu.direccion" />
               
               <s:hidden name="usu.estado" value="%{clonUsu.estado}"  />
               <s:submit value="Actualizar usuario" />
           </s:form>    
            
        </s:elseif>
        
        
        <s:elseif test="%{datos=='prod'}">
            <h1>Edici�n de productos</h1>
            <s:a cssClass="botonBonito" onclick="abrirAdminCat()" href="#">Administrar categorias</s:a>
               <s:a cssClass="botonBonito" onclick="abrirAdminArea()" href="#">Administrar �reas</s:a>
               
             <s:form name="FormuProdAct" action="guardarCambiosProd" enctype="multipart/form-data" method="post" >
              <s:hidden name="prod.id" value="%{clonProd.id}" />
                 <s:textfield onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Nombre" value="%{clonProd.nombre}" name="prod.nombre" />
               <s:textarea onKeyUp='this.value=this.value.toUpperCase()' required='true' label="Descripci�n"  cols="50" rows="7" value="%{clonProd.descripcion}" name="prod.descripcion" />
               <s:file label="�Cambiar imagen?" name="imagen" />
               <s:hidden name="prod.imagen" value="%{clonProd.imagen}"   />
               <s:textfield required='true' label="Precio" value="%{clonProd.precio}" name="prod.precio" />
               
               <s:hidden name="prod.estado" value="%{clonProd.estado}"   />
               <s:select label="Tipo de men�" name="prod.tipoMenu" 
                        headerKey="%{clonProd.tipoMenu}" value="%{clonProd.tipoMenu}"
                        list="#@java.util.LinkedHashMap@{'1':'Desayuno',
                                                         '2':'Comida',
                                                         '3':'Cena'}"  />
                <s:div id="contieneComboCat">
                    <s:select
                        list="listaCategorias"
                        name="prod.idCategoria.id"
                        listKey="id"
                        listValue="%{nombre}"
                        label="Categoria"
                        headerKey="%{clonProd.idCategoria.id}" 
                        headerValue="%{clonProd.idCategoria.nombre}"
                    />
               </s:div> 
               
               <s:select
                    list="listaAreas"
                    name="prod.idArea.id"
                    listKey="id"
                    listValue="%{nombre}"
                    label="�rea"
                    headerKey="%{clonProd.idArea.id}" 
                    headerValue="%{clonProd.idArea.nombre}"
                />
              
               <s:submit value="Actualizar producto" />
           </s:form>    
            
               
                <s:div id="adminCat" cssClass="flotante">
                   <h2>Administraci�n de categor�as<a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>   
                   Atenci�n. Al borrar una categor�a los productos pertenecientes a ella misma ya no podr�n ser a�adidos a una orden.<br>
                   <div style="margin-top: 8px;" id="contieneTablaCat">
                   <table class="tablaGrande invertirColor" >
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator value="listaCategorias"> <!--esto corresponde al atributo DTO-->
                        <tr>
                            <td>${nombre} </td>
                            <td>
                                <a class="botonBonito" onclick="eliminarCategoria(${id})" href="#">Eliminar</a>    
                            </td>
                        </tr>
                        </s:iterator>
                    </tbody>
                </table>
                       </div>
                   <br>
                   <s:textfield id="nombreNuevaCat" label="Nueva categor�a" /> 
                   <div style="float: right; margin-top: 5px;">
                   <s:a cssClass="botonBonito" onclick="agregarCat()" href="#">Agregar categor�a</s:a>
                   </div>
                   
               </s:div>    
                   
               <s:div id="adminArea" cssClass="flotante">
                   <h2>Administraci�n de �reas<a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>
                   <div style="margin-top: 8px;" id="contieneTablaArea">
                   <table class="tablaGrande invertirColor" >
                    <thead>
                        <tr>
                            <th>NOMBRE</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator value="listaAreas"> <!--esto corresponde al atributo DTO-->
                        <tr>
                            <td>${nombre} </td>
                            <td>
                                <a class="botonBonito" onclick="eliminarArea(${id})" href="#">Eliminar</a>    
                            </td>
                        </tr>
                        </s:iterator>
                    </tbody>
                </table>
                       </div>
                   <br>
                   <s:textfield id="nombreNuevaArea" label="Nueva �rea" /> 
                   <div style="float: right; margin-top: 5px;">
                   <s:a cssClass="botonBonito" onclick="agregarArea()" href="#">Agregar �rea</s:a>
                   </div>
                   
               </s:div>      
               
               
        </s:elseif>
  </s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>                  
        <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 
   </s:div>     
<jsp:include page="pie.jsp" />        
