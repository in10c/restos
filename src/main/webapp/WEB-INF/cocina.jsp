<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
    <s:div id="fechaTablero">
        <div><%
        SimpleDateFormat formateador = new SimpleDateFormat(
       "dd 'de' MMMM 'de' yyyy", new Locale("ES"));
       Date fechaDate = new Date();
       String fecha = formateador.format(fechaDate);
       out.println(fecha);
        %>  </div>
        <div id="hora"></div>
    </s:div>
   
    <div id="conOrdenes" style="display:block; position: fixed; width: 90%; top:18%; left: 5%; height:68%; overflow-x: auto; overflow-y: auto">
        
    </div>
    
    
    <s:div id="rutaAbajo">
            <div class="cambiador">
                    <a href="irMenu.action">
                            <span style="text-align:center;">
                                    <img src="<%=request.getContextPath()%>/imagenes/home.png" width="40" height="40">
                            </span>
                    </a>
            </div>
    </s:div>    

    <script>
        $(document).on("ready", iniciar);
        
        function iniciar(){
            startTime();
            llamarOrdenes();
            setInterval(relojes,1000);
            setInterval(llamarOrdenes,10000);
        }
        
        function rayar(){
            $(this).parent("span").parent("li").css("text-decoration", "line-through");
        }
        
        function llamarOrdenes(){
         salida = $("#conOrdenes");
         $.ajax({
            type: "POST",   
            dataType: "json",
            url:"actualizaOrdenes.action",
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               salida.html(data.salida);
               $(".tachaLinea").on("click", rayar);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
        }
        
        function relojes(){
            $(".contadores").each(function(){
                
                minutos = parseInt($(this).find("span.minu").text());
                segundos = parseInt($(this).find("span.secu").text());
                siguienteSegundos = segundos+1;
                if (siguienteSegundos==60){
                        siguienteSegundos=0;
                        siguienteMinutos = minutos+1;
                } else {
                        siguienteMinutos = minutos;
                }

                $(this).find("span.minu").text(siguienteMinutos);
                $(this).find("span.secu").text(siguienteSegundos);
            }) 
	}
    </script>    
    
        
        
<jsp:include page="pie.jsp" />    
