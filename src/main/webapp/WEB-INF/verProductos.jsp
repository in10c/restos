<%@taglib prefix="s" uri="/struts-tags" %>
<jsp:include page="cabezal.jsp" />
<s:div cssClass="contenedor">
    <s:if test="usuario_sesion.tipo==1">
        <h1>Lista de productos</h1>
        
        <s:if test="productos.size>0">
            <table class="tablaGrande">
                <thead>
                    <tr>
                        <th>IMAGEN</th>
                        <th>NOMBRE</th>
                        <th>TIPO DE MEN�</th>
                        <th>CATEGOR�A</th>
                        <th>�REA DE COCINA</th>
                        <th>PRECIO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="productos"> <!--esto corresponde al atributo DTO-->
                    <tr>
                        <td> <img width="100" height="100" src="<%=request.getContextPath()%>/subidas/fotosProd/${imagen}"/>
                             </td>
                        <td>${nombre}</td>
                        <s:if test="%{tipoMenu==2}">
                            <td>Comida</td>
                        </s:if> 
                        <s:elseif test="%{tipoMenu==1}">
                            <td>Desayuno</td>
                        </s:elseif>
                        <s:elseif test="%{tipoMenu==3}">
                            <td>Cena</td>
                        </s:elseif>
                        <td>${idCategoria.nombre}</td>
                        <td>${idArea.nombre}</td>
                        <td>${precio}</td>
                        <td>
                        <s:a cssClass="botonBonito" href="editarProd.action?id=%{id}">Editar</s:a>
                        <s:a cssClass="botonBonito" onclick="javascript:return confirm('�Est�s seguro de querer eliminar este registro?');" href="eliminarProd.action?id=%{id}">Eliminar</s:a>    
                        </td>
                    </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:if>
        <br>
        <s:a cssClass="botonBonito link" href="formRegProd.action">Registrar platillo</s:a>    
</s:if>           
 <s:else>No tienes permiso para ver esto.</s:else>
        <s:a cssClass="botonBonito link" href="irMenu.action"><img src="imagenes/curva.png"  width="30" /> Regresar al men� principal</s:a> 

</s:div>     
<jsp:include page="pie.jsp" />        

<script type="text/javascript">
	$(document).on("ready", iniciar);
	
	function iniciar () {
		$('.tablaGrande').dataTable({
                    "bJQueryUI": false,
                    "sPaginationType": "full_numbers"
                });
	}

</script>