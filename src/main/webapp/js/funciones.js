/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




function agregarCat (){
    var nombreCat = $("#nombreNuevaCat").val();
    if(nombreCat===""){
        alert("El campo de nombre est?? vacio, ll??nalo antes de agregar una categor??a.");
    } else {

        $.ajax({
            type: "POST",   
            dataType: "json",
            url:"agregarCat.action",
            data:"nombre="+nombreCat,
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               $("#contieneTablaCat").html(data.tabla);
               $("#registrarProd_prod_idCategoria_id").parent("td").html(data.select);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
    }
}

function editarCategoria (id) {
    alert(id);
}

function eliminarCategoria (id) {
    if(confirm('??Est??s seguro de querer eliminar este registro?')){
        $.ajax({
            type: "POST",   
            dataType: "json",
            url:"eliminarCat.action",
            data:"id="+id,
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               $("#contieneTablaCat").html(data.tabla);
               $("#registrarProd_prod_idCategoria_id").parent("td").html(data.select);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
    }
}

function entregar (id) {
        $.ajax({
            type: "POST",   
            dataType: "json",
            url:"entregarOrden.action",
            data:"idOrden="+id,
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               salida.html(data.salida);
               $(".tachaLinea").on("click", rayar);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
}



function cerraryActualizar() {
    $("#adminCat").hide();
    $("#adminArea").hide();
    $("#selectorMesas").hide();
    $("#selectorMenu").hide();
    $("#selectorCategorias").hide();
    $("#ModCantidades").hide(); 
    $("#selectorClientes").hide();
    $("#alerta").hide();
}

function abrirAdminCat() {
    $("#adminCat").show();
}

function abrirSelecCli() {
    $("#selectorClientes").show();
}

function abrirSelMesas() {
    $("#selectorMesas").show();
}

function abrirAdminArea() {
    $("#adminArea").show();
}

function abrirSelectorCategorias(){
    $("#selectorCategorias").show();
} 


function abrirSelectorMenu() {
    $("#selectorMenu").show();
}

function abreModCantidad(numElem) {
    $("#ModCantidades").html('<h2>Modificar Elemento <a style="float: right;" onclick="cerraryActualizar()" href="#"><img src="imagenes/cerrar.png"  width="30" /></a></h2>'
    +'<span id="elementos" style="font-size:2.3em;"><a href="#" onclick="menosCantidad('+numElem+')"><i class="fa fa-minus-circle"></i></a>'
    +' <a href="#" onclick="masCantidad('+numElem+')"><i class="fa fa-plus-circle"></i></a> <a href="#" onclick="eligeCantidad('+numElem+')"><i class="fa fa-pencil"></i></a> <a href="#" onclick="eliminarElemento('+numElem+')"><i class="fa fa-times-circle"></i></a></span>');
    $("#ModCantidades").show();   
}

function agregarArea (){
    var nombreCat = $("#nombreNuevaArea").val();
    if(nombreCat===""){
        alert("El campo de nombre est?? vacio, ll??nalo antes de agregar una nueva ??rea.");
    } else {

        $.ajax({
            type: "POST",   
            dataType: "json",
            url:"agregarArea.action",
            data:"nombre="+nombreCat,
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               $("#contieneTablaArea").html(data.tabla);
               $("#registrarProd_prod_idArea_id").parent("td").html(data.select);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
    }
}

function eliminarArea (id) {
    if(confirm('??Est??s seguro de querer eliminar este registro?')){
        $.ajax({
            type: "POST",   
            dataType: "json",
            url:"eliminarArea.action",
            data:"id="+id,
            async: false,
            beforeSend: function() {
            },
            success: function(data) {
               $("#contieneTablaArea").html(data.tabla);
               $("#registrarProd_prod_idArea_id").parent("td").html(data.select);
            },
            error: function(jqXHR, error, errorThrown) {
                ok=0;
            }
        });
    }
}

function aplicarMesa(id, nombre) {
    //alert("id es "+id+" nombre es "+nombre);
    $("#mesaSeleccionada").html(nombre);
    $("#mesaSeleccionada").attr("data-id", id);
    $("#selectorMesas").hide();
}

// Funciones pizarra

function eliminarElemento(numElem) {
    $("#elem"+numElem).remove();
    $("#ModCantidades").hide();
    sumar();
}

function masCantidad(numElem) {
    datCantidad = $("#elem"+numElem).find("td.cantidades");
    var cantidad = parseInt(datCantidad.text());
    datCantidad.text((cantidad+1));
    multiplica(numElem);
    sumar();
}

function eligeCantidad(numElem) {
    var cantidadAntes = prompt("Escribe una cantidad");
    datCantidad = $("#elem"+numElem).find("td.cantidades");
    if(!isNaN(cantidadAntes)){
        datCantidad.text(cantidadAntes);
        multiplica(numElem);
        sumar();
    }
}

function menosCantidad(numElem) {
    datCantidad = $("#elem"+numElem).find("td.cantidades");
    var cantidad = parseInt(datCantidad.text());
    if(cantidad!==1){
        datCantidad.text((cantidad-1));
        multiplica(numElem);
        sumar();
    }
}

function multiplica(sel) {
    //alert(numElem);
    datCantidad = $("#elem"+sel).find("td.cantidades");
    var cantidad = parseInt(datCantidad.text());
    
    datPrecio = $("#elem"+sel).find("td.precios");
    var prec = parseInt(datPrecio.text());
    console.log(prec);
    $("#elem"+sel).find("td.totales").text((cantidad*prec));
}


function ponerBotones () {
    var contador= $(this).attr("data-contador");
    $("#wrap"+contador).show();
    $(this).off("click");
}

var elemIndex= 0;

function agregarAOrden(posicion) {
    producto = $("#prod"+posicion);
    $("#wrap"+posicion).hide();
    elemIndex+=1;
    //producto.child("div.wraperBotones").remove();
    $("#elemCuentaTabla").append("<tr class='elemento' data-id='"+producto.attr("data-id")+"' id='elem"+elemIndex+"'><td class='nombres'><a onclick='abreModCantidad("+elemIndex+")' href='#'>"
            +producto.attr("data-nombre")+"</a></td><td class='precios'>"+producto.attr("data-precio")+"</td><td class='cantidades'>1</td><td class='totales'>"+producto.attr("data-precio")+"</td></tr>");
    //producto.on("click", ponerBotones);
    sumar();   
}



function sumar() {
    var impuesto = 1.16;
    var total =0;
    $("#elemCuentaTabla tr td.totales").each(function(){
          total += parseInt($(this).text()); 
    });
    $("#cantSubtotal").text((Math.round(total * 100) / 100)+" MXN");
    $("#cantTotal").text((Math.round((total*impuesto) * 100) / 100)+" MXN");
}

//reloj de cocina

function startTime()
{
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    // add a zero in front of numbers<10
    m=checkTime(m);
    s=checkTime(s);
    document.getElementById('hora').innerHTML=h+":"+m+":"+s+" pm";
    t=setTimeout(function(){startTime()},500);
}

function checkTime(i)
{
    if (i<10)
      {
      i="0" + i;
      }
    return i;
}