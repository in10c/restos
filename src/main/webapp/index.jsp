<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="css/estilo.css"/>  
        <link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
       
    </head>
    <body>
<s:div cssClass="cajaLogin">
    <h1>Acceso a RestOS</h1>
        <s:actionerror cssStyle="color:red; size:12px;" />
        <s:form action="login" name="login" id="loginForm" method="POST" namespace="/"  >
            <s:textfield name="usuario.usuario" label="Usuario" />
            <s:password name="usuario.contrasena" label="Password" />
            <s:submit value="Entrar al Sistema" />            
        </s:form>
</s:div>
        
        
    </body>
</html>